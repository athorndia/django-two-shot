from django.contrib.auth import (
    authenticate,
    login,
    logout,
)
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LoginForm


# Create your views here.


'''
This feature 7 is about setting up a login page
so that the person using the application can be identified.
'''


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                form.add_error(None, 'Invalid username or password')
    else:
        form = LoginForm()
    context = {
        'form': form,
    }
    return render(request, 'accounts/login.html', context)


# Feature 9: logs a person out then redirects them to
# the URL path registration "login"
def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            # first_name = form.cleaned_data['first_name']
            # last_name = form.cleaned_data['last_name']

            if password == password_confirmation:
                # Create a new user with those values and save it
                # to a variable:
                # https://docs.djangoproject.com/en/4.1/topics/auth/default/#creating-users
                user = User.objects.create_user(
                    username=username,
                    password=password,
                    # first_name=first_name,
                    # last_name=last_name,
                )

                # Login the user with the user you just created:
                # https://docs.djangoproject.com/en/4.1/topics/auth/default/#how-to-log-a-user-in
                # If the account is created, then user is redirect them
                # the list of projects ("home")
                login(request, user)
                return redirect("home")

            # Add error message for the person trying to sign up that tells
            # them if their passwords don't match if they type them in wrong.
            else:
                form.add_error("password", "Passwords do not match")
        # else:
        #     form = SignUpForm()
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
