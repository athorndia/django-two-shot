from django.db import models
from django.contrib.auth.models import User


# STEP 1: Create your models here.
# These models are for user accounts!

'''
Models are used to define the structure of your application's data.
They're defined as Python classes that inherit from django.db.models.Model
and define fields that represent the data you want to store in your database.

When a model is defined, Django creates a database table for that model.
Each field in the model is represented as a column in the database table.
Django ORM (Object-Relational Mapping) allows you to interact with the
database (read, create, update, and delete records) and perform
operations on the data stored in the table.
'''


'''
The ExpenseCategory model is a value that we can apply to receipts
like "gas" or "entertainment".
'''


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User,
        related_name='categories',
        on_delete=models.CASCADE
        )

    def __str__(self):
        return self.name


'''
The Account model is the way that we paid for it,
such as with a specific credit card or a bank account.

The number property is intended to hold a string,
so it can allow the possibility of non-numeric
characters in the account number.
AKA provides flexibility and is easier to work with.
'''


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name='accounts',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


'''
The Receipt model is the primary thing that this
application keeps track of for accounting purposes.

ALWAYS store money in DecimalField!
'''


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(
        max_digits=10,
        decimal_places=3
        )
    tax = models.DecimalField(
        max_digits=10,
        decimal_places=3
        )
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User,
        related_name='receipts',
        on_delete=models.CASCADE
        )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name='receipts',
        on_delete=models.CASCADE
        )
    account = models.ForeignKey(
        Account,
        related_name='receipts',
        on_delete=models.CASCADE,
        null=True
        )
