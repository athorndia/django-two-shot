from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


# STEP 3: Register your models here.

'''
Here we register the TodoList model with the admin
so that we can see it in the Django admin site.
We also define a custom display for each field of our todo list
using `list_display`.
We display the name and the id as columns in the model's
list display.
'''


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'owner',
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'number',
        'owner',
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        'vendor',
        'total',
        'tax',
        'date',
        'purchaser',
        'category',
        'account',
    )
